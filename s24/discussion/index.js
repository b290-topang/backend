//https://www.w3schools.com/js/js_es6.asp
//SECTION: E X P O N E N T  O P E R A T O R

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

//SECTION: T E M P L A T E  L I T E R A L S
/*
	- Allows to write strings without using the concatenation operator (+)
	- Greatly helps with code readability
*/

let name = "John";

// Pre-template literal string
// Uses single/double quotes ('' / "")

let message = "Hello " + name + "! Welcome to programming!";
console.log("Message w/o template literals: " + message);

// Strings using template literals
// Uses backticks (``)

message = `Hello ${name}! Welcome to programming!`;
console.log(`Message w/ template literals: ${message}`);

// Multi-line using template literals
const anotherMessage = `
${name} attended a math competition.powHe won it by solving the probvlem 8 ** 2 with the solution of ${firstNum}.`;
console.log(anotherMessage);

/*
	- Template literals allow us to write strings with embedded JavaScript expressions
	- expressions are any valid unit of code that resolves to a value
	- "${}" are used to include JavaScript expressions in strings using template literals
*/

const interestRate = 0.1;
const principal = 1000;

console.log(`The interest on your savings account is ${principal * interestRate}`);

//SECTION: A R R A Y  D E S T R U C T U R I N G
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability
	- Syntax
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName}`);

// Array Destructuring

const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}`);

//SECTION: O B J E C T  D E S T R U C T U R I N G
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	- Syntax
		let/const {propertyName, propertyName, propertyName} = object;
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Object Destructuring

const { givenName, maidenName, familyName } = person;

console.log(givenName);
console.log(middleName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName({givenName, maidenName, familyName}) {
	console.log(`${givenName} ${maidenName} ${lastName}`);
}

getFullName(person);

//SECTION: A R R O W  F U N C T I O N S
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

/*
	Syntax
		const variableName = () => {
		console.log()
	}
*/

const hello = () => {
	console.log("Hello World");
}

// Pre-Arrow function and template literals

/*function printFullName (firstName, middleInitial, lastName) {
	console.log (firstName + " " + middleInitial + " " lastName);
}*/

// Arrow Function
/*
	- Syntax
		let/const variableName = (parameterA, parameterB, parameterC) => {
			console.log();
		}
*/

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

console.log("John", "D", "Smith");

const students = ["Owen", "Moon", "Blessy", "Gracia"];

// Arrow Functions with Loops
// Pre-Arrow function

/*students.forEach(function(student){
	console.log(`${student} is a student`);
});*/

students.forEach(student => {console.log(`${student} is a student`);});

//SECTION: I M P L I C I T  R E T U R N  S T A T E M E N T 
// The function is only used in the "forEach" method to print out a text with the student's names
/*
	- There are instances when you can omit the "return" statement
	- This works because even without the "return" statement JavaScript implicitly adds it for the result of the function
*/
// Arrow Function in a code block
/*const add = (x , y) => {
	return x + y;
}

let total = add(1, 2);
console.log(total);*/

// Arrow Function in One-Line
const add = (x, y) => x + y;

let total = add(1, 2);
console.log(total);

//SECTION: D E F A U L T  F U N C T I O N  A R G U M E N T  V A L U E
const greet = (name = "User") => {
	return `Good morning, ${name}!`;
}

console.log(greet("Nezuko"));

//SECTION: C L A S S - B A S E D  O B J E C T  B L U E P R I N T S
/*
	- Allows creation/instantiation of objects using classes as blueprints
*/
// Creating a Class
/*
	- The constructor is a special method of a class for creating/initializing an object for that class.
	- The "this" keyword refers to the properties of an object created/initialized from the class
	- By using the "this" keyword and accessing an object's property, this allows us to reassign it's values
- Syntax
		class className {
			constructor(objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}

};

// Instantiating an Object
/*
	- The "new" operator creates/instantiates a new object with the given arguments as the values of it's properties
	- No arguments provided will create an object without any values assigned to it's properties
	- let/const variableName = new ClassName();
*/

let myCar = new Car();

/*
	- Creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't re-assign it with another data type
	- It does not mean that it's properties cannot be changed/immutable
*/

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);	

// Creating/instantiating a new object from the car class with initialized values
const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);