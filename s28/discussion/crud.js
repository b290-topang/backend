// CRUD Operations

// SECTION: INSERTING DOCUMENTS (Create)
/*
	Syntax:
		db.collectionName.insertOne({object})
*/
// Inserting a single document
db.users.insertOne(
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "09123456789",
			email: "janedoe@mail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	}

);

/*
	Syntax: 
		db.collectionName.insertMany([{objectA}, {objectB}]);
*/
// Inserting multiple documents
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "09123456789",
			email: "stephenhawking@mail.com"
		},
		courses: ["Phyton", "React", "PHP"],
		department: "none"
	},

	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "09123456789",
			email: "neilarmstrong@mail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}

]);

// SECTION: Finding documents (Read)
/*
	Syntax:
		db.collectionName.find();
*/
// Leaving the search criteria empty will retrieve all the documents
db.users.find();
db.users.findOne({ firstName: "Stephen"});

// Finding documents with a single parameters
db.users.find(
	{
		firstName: "Stephen"
	}

);

// Finding documents with multiple parameters
db.users.find(
		{
			lastName: "Armstrong",
			age: 83
		}

	);

// SECTION: Updating documents (Update)
// Updating a single document
/*
	Syntax:
		db.collectionName.updateOne(
			{
				criteria
			}, 

			{
				$set: 
					{
						field: value
					}
			}
		)
*/
db.users.insertOne(
	{
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contact: {
			phone: "00000000000",
			email: "test@mail.com"
		},
		courses: [],
		department: "none"
	}

);

// updateOne will only update the first document that matches the search criteria
db.users.updateOne(
	{ firstName: "Test" },

	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}

);

// Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany({criteria} {});
*/
	db.users.updateMany(
		{ department: "none" },

		{
			$set { department: "HR" }
		}

);

// Replace One
	db.users.replaceOne (
			{ firstName: "Bill" },
			{
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "09123456789",
					email: "bill@gmail.com"
				},
				courses: ["PHP", "Laravel", "HTML"],
				department: "Operations",
			}

		);


// SECTION: Deleting documents (Delete)
db.users.insertOne(
	{
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contact: {
			phone: "00000000000",
			email: "test@mail.com"
		},
		courses: [],
		department: "none"
	}

);

// Deleting a single document
/*
	Syntax:
	 db.collectionName.delete(
	  { criteria }

	 )
*/

db.users.deleteOne(
 { firstName: "Test" }

);

// Deleting many
/*
	Syntax:
	 db.collectionName.deleteMany(
	  { criteria }

	 )
*/

db.users.deleteMany(
 { firstName: "Bill" }

)

// SECTION: Advanced queries

db.users.find ({
 contact: {
 	phone: "09123456789",
 	email: "stephenhawking@mail.com"
 }
})

db.users.find ({
 "contact.email": "janedoe@mail.com"
})

// Querying an array with exact elements
db.users.find ({
 courses: ["CSS", "JavaScript", "Python"]

})

db.users.find({
	courses: {
		$all: ["React", "Python"]
	}

})

db.users.insertOne({
	namearr: [
		 {
		 	namea: "Juan"
		 },

		 {
		 	nameb: "Tamad"
		 }
		 
		]
})