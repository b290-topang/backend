// [Section] Javascript Synchronous vs Asynchronous

// Javascript is by default is synchronous meaning that only one statement is executed at a time

console.log("Hello");
console.log("Bye");

/*for (let i = 0; i <= 1500; i++) {
	console.log(i);

}*/
console.log("Hello");

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// [Section] Getting all posts

// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

/*
	Syntax:
		fetch("URL");
*/
console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
fetch("https://jsonplaceholder.typicode.com/posts")

// The "fetch" method will return a "promise" that resolves to a "Response" object
// The "then" method captures the "Reponse" object and returns another "promise" which will eventually be "resolved" or "rejected"
.then(response => console.log(response.status));

fetch("https://jsonplaceholder.typicode.com/posts")
	// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
	.then(response => response.json())
	// Print the converted JSON value from the "fetch" request
	// Using multiple "then" methods creates a "promise chain"
	.then(json => console.log(json));

	// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
	// Used in functions to indicate which portions of code should be waited for

	/*async function fetchData(){

		// waits for the "fetch" method to complete then stores the value in the "result" variable
		let result = await fetch ("https://jsonplaceholder.typicode.com/posts");
		console.log(result);
		console.log(typeof result);

		console.log(result.body);

		let json = await result.json();
		console.log(json);

		let result = await fetch ("https://jsonplaceholder.typicode.com/posts");
		let json = await result.json();


	}

	fetchData();*/

// [Section] Getting a specific post
// Retrieves a specific post a following the REST API
// (/posts/:id, GET)
fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then(response => response.json())
	.then(json => console.log(json));

	async function fetchData() {
		let result = await fetch("https://jsonplaceholder.typicode.com/posts/2");
		let json = await result.json();
		console.log(json);
	}

	fetchData();

// [Section] Creating a post
/*
	Syntax:
		fetch("URL", options)
		.then(response => {})
		.then(response => {})
*/

	// Create a new post following the REST API ( /post/:userId)
	fetch("https://jsonplaceholder.typicode.com/posts",
		{
			// Sets the method of the "Request" object to "POST" following REST API
			// Default method is GET
			method: "POST",
			// Sets the header data of the "Request" object to be sent to the backend
			// Specified that the content will be in a JSON structure
			headers: {
				"Content-Type": "application/json"
			},

			body: JSON.stringify({
				title: "New post",
				body: "Hello World!",
				userId: 1

			})
		}
	).then(response => response.json())
	 .then(json => console.log(json));

// [Section] Updating a post

	 fetch("https://jsonplaceholder.typicode.com/posts/1", {

	 	method: "PUT",
	 	headers: { "Content-Type": "application/json" },
	 	body: JSON.stringify({
	 		id: 1,
	 		title: "Update post",
	 		body: "Hello again",
	 		userID: 1
		})

	 }).then(response => response.json()).then(json => console.log(json));


// [Section] Patch a post

	 fetch("https://jsonplaceholder.typicode.com/posts/1", {

	 	method: "PATCH",
	 	headers: { "Content-Type": "application/json" },
	 	body: JSON.stringify({
	 		id: 1,
	 		body: "Hello again and again"

		})

	 }).then(response => response.json()).then(json => console.log(json));

// [Section] Delete a post

	 fetch("https://jsonplaceholder.typicode.com/posts/1", {

	 	method: "DELETE"

	 }).then(response => response.json()).then(json => console.log(json));


// [Section] Filtering Posts
// The data can be filtered by sending the userId along with the URL
// Information sent vie the url can be done by adding the question mark symbol (?)
/*
	Syntax:
		Individual Parameter
		 "url?parameterName=value"

		Multiple Parameter
		 "url?parameterA=valueA&parameterB=valueB"
*/

	 fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
	  .then(response => response.json()).then(json => console.log(json));

	 fetch("https://jsonplaceholder.typicode.com/posts?userId=1&userId=2")
	  .then(response => response.json()).then(json => console.log(json));

// [Section] Retrieving nested/related comment to posts
	
	fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
	 .then(response => response.json()).then(json => console.log(json));

async function createToDo(){

return await (

   //Add fetch here.
  
  fetch('https://jsonplaceholder.typicode.com/posts', 
     {
     method: "POST",
     headers: {
        "Content-Type": "application/json"
     },
        body: JSON.stringify({
           title: "Created To Do List Item",
           userId: 1

        })
     }
     
  ).then(response => response.json())
   .then(json => { console.log(json) })
);

}