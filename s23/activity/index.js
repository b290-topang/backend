// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
	/*let trainer = {
		name: "Ash Ketchum",
		age: 10,
		pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
		friends: {
			hoenn: ["May", "Max"],
			kanto: ["Brock", "Misty"]
		},
		talk: function() {
			return "Pikachu! I choose you!";
		}
	}*/

	let trainer = {};
	
// Initialize/add the given object properties and methods
// Properties
	trainer.name = "Ash Ketchum";
	trainer.age = 10;
	trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
	trainer.friends = {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	}
// Methods
	trainer.talk = function() {
		return "Pikachu! I choose you!";
	}
// Check if all properties and methods were properly added
	console.log(trainer);

// Access object properties using dot notation
	console.log("Result for the dot notation: ");
	console.log(trainer.name);

// Access object properties using square bracket notation
	console.log("Result for the square bracket notation: ");
	console.log(trainer["pokemon"]);

// Access the trainer "talk" method
	console.log("Result of talk method");
	console.log(trainer.talk());

// Create a constructor function called Pokemon for creating a pokemon
	function Pokemon(name, level) {
		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// Pokemon Movesets
		
		this.faint = function(target) {
			return target.name + " fainted.";
		}

		this.tackle = function(target) {
			/*console.log(this.name + " tackled " + target.name);	
			target.health = target.health - this.attack;
			let message = target.name + "'s health is now reduced to " + target.health;

			if (target.health <= 0) {
				console.log (message);
				return this.faint(target);
			} 
			else {
				return message;
			}	*/

			for (let index = 0; index < target.health; index++)
			{
				target.health = target.health - this.attack;

				if (target.health <= 0) {
					console.log (target.name + "'s health is now reduced to " + target.health)
					return this.faint(target);
				} 
				else {
					return target.name + "'s health is now reduced to " + target.health;
				}

			}
		}




	}

// Create/instantiate a new pokemon
	let pokemon1 = new Pokemon("Pikachu", 12);
	console.log(pokemon1);

// Create/instantiate a new pokemon
	let pokemon2 = new Pokemon("Geodude", 8);
	console.log(pokemon2);

// Create/instantiate a new pokemon
	let pokemon3 = new Pokemon("Mewtwo", 100);
	console.log(pokemon3);

// Invoke the tackle method and target a different object
	console.log(pokemon2.tackle(pokemon1));
	console.log(pokemon1);

// Invoke the tackle method and target a different object
	console.log(pokemon3.tackle(pokemon2));
	console.log(pokemon2);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}