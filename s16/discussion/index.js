//=============================
//=============================
//J A V A S C R I P T  F I L E
//=============================
//=============================

//console.log("uwu");

// SECTION: A R I T H M E T I C  O P E R A T O R S
	let x = 1397;
	let y = 7831;

	//MINI ACTIVITY (+, - , *, /)

	//Addition
	let sum = x + y;	
	console.log("The result of addition operator: " + sum);

	//Substraction
	let diff = x - y;
	console.log("The result of substraction operator: " + diff);

	//Multiplication
	let prod = x * y;
	console.log("The result of multiplication operator: " + prod);

	//Division
	let quot = x / y;
	console.log("The result of division operator: " + quot);

	//Modulo (Remainder)
	let modu = y % x;
	console.log("The result of modulo operator: " + modu);

//SECTION: A S S I G N M E N T  O P E R A T O R S
	//The assignment operator assigns the value of the **right** operand to a variable.
	//Basic Assignment Operator (=)
	let assignmentNumber = 8;

	//The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	//MINI ACTIVITY
	// Have the Addition, Substraction, Multiplication, Division, Modulo

	//Addition Assignment Operator (+=)
	 //Common Way for assignment
	  assignmentNumber = assignmentNumber + 2;
	  console.log("Result of addition assignment operator: " + assignmentNumber);
	 //Shorthand for assignment
	  assignmentNumber += 2;
	  console.log("Result of addition assignment operator: " + assignmentNumber);

	//Substraction Assignment Operator (-=)
	 //Common Way for assignment
	  assignmentNumber = assignmentNumber - 2;
	  console.log("Result of substraction assignment operator: " + assignmentNumber);
	 //Shorthand for assignment
	  assignmentNumber -= 2;
	  console.log("Result of substraction assignment operator: " + assignmentNumber);

	//Multiplication Assignment Operator (*=)
	 //Common Way for assignment
	  assignmentNumber = assignmentNumber * 2;
	  console.log("Result of multiplication assignment operator: " + assignmentNumber);
	 //Shorthand for assignment
	  assignmentNumber *= 2;
	  console.log("Result of multiplication assignment operator: " + assignmentNumber);

	//Division Assignment Operator (/=)
	 //Common Way for assignment
	  assignmentNumber = assignmentNumber / 2;
	  console.log("Result of division assignment operator: " + assignmentNumber);
	 //Shorthand for assignment
	  assignmentNumber /= 2;
	  console.log("Result of division assignment operator: " + assignmentNumber);

	//Modulo Assignment Operator (%=)
	 //Common Way for assignment
	  assignmentNumber = assignmentNumber % 2;
	  console.log("Result of modulo assignment operator: " + assignmentNumber);
	 //Shorthand for assignment
	  assignmentNumber %= 2;
	  console.log("Result of modulo assignment operator: " + assignmentNumber);

	// Multiple Operators and Parentheses
	 /*
		- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
		- The operations were done in the following order:
			1. 3 * 4 = 12
			2. 12 / 5 = 2.4
			3. 1 + 2 = 3
			4. 3 - 2.4 = 0.6
	 */
	  let mdas = 1 + 2 - 3 * 4 / 5;
	  console.log("Result of mdas operation: " + mdas);


	 /*
        - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
        - The operations were done in the following order:
            1. 4 / 5 = 0.8
            2. 2 - 3 = -1
            3. -1 * 0.8 = -0.8
            4. 1 + -.08 = .2
     */
	  let pemdas = 1 + (2 - 3) * (4 / 5);
	  console.log("Result of pemdas operation: " + pemdas);


	 /*
        - By adding parentheses "()" to create more complex computations will change the order of operations still following the same rule.
        - The operations were done in the following order:
            1. 4 / 5 = 0.8
            2. 2 - 3 = -1
            3. 1 + -1 = 0
            4. 0 * 0.8 = 0
     */
	  pemdas = (1 + (2 - 3)) * (4 / 5);
	  console.log("Result of pemdas operation: " + pemdas);

	//Increment and Decrement
	//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to
	 let z = 1;
	 let increment = ++z;

	 console.log("Result of pre-increment: " + increment);
	 console.log("Result of pre-increment: " + z);

	// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
	// The value of "z" was also increased even though we didn't implicitly specify any value reassignment

	 increment = z++;

	 console.log("Result of post-increment: " + increment);
	 console.log("Result of post-increment: " + z);

	// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
	// The value of "z" is at 2 before it was incremented

	 let decrement = --z;

	 console.log("Result of pre-decrement: " + decrement);
	 console.log("Result of pre-decrement: " + z);

	// The value of "z" is decreased by a value of one before returning the value and storing it in the variable "decrement"
	// The value of "z" is at 3 before it was decremented
	// The value of "z" was decreased reassigning the value to 2

	 decrement = z--;

	 console.log("Result of post-decrement: " + increment);
	 console.log("Result of post-decrement: " + z);

	// The value of "z" is returned and stored in the variable "increment" then the value of "z" is decreased by one
	// The value of "z" is at 2 before it was decremented
	// The value of "z" was decreased reassigning the value to 1

//SECTION: C O M P A R I S O N  O P E R A T O R S
	let juan = 'juan';

	//(Loose) Equality Operator (==)
	/* 
        - Checks whether the operands are equal/have the same content
        - Attempts to CONVERT AND COMPARE operands of different data types
        - Returns a boolean value
    */
    console.log("Start of Loose Equality Operator");
	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == "1");
	console.log(0 == false);
	console.log('juan' == 'juan');
	console.log('juan' == juan);

	//(Strict) Equality Operator (===)
	/* 
        - Checks whether the operands are equal/have the same content
        - Also COMPARES the data types of 2 values
        - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
        - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
        - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
        - Strict equality operators are better to use in most cases to ensure that data types provided are correct
    */
    console.log("Start of Strict Equality Operator");
	console.log(1 === 1);
	console.log(1 === 2);
	console.log(1 === "1");
	console.log(0 === false);
	console.log('juan' === 'juan');
	console.log('juan' === juan);

	//(Loose) Inequality Operator (!=)
	/* 
        - Checks whether the operands are not equal/have different content
        - Attempts to CONVERT AND COMPARE operands of different data types
    */
    console.log("Start of Loose Inequality Operator");
	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != "1");
	console.log(0 != false);
	console.log('juan' != 'juan');
	console.log('juan' != juan);

	//Strict Inequality Operator
	/* 
        - Checks whether the operands are not equal/have the same content
        - Also COMPARES the data types of 2 values
    */
    console.log("Start of Strict Inequality Operator");
	console.log(1 !== 1);
	console.log(1 !== 2);
	console.log(1 !== "1");
	console.log(0 !== false);
	console.log('juan' !== 'juan');
	console.log('juan' !== juan);

//SECTION: R E L A T I O N A L  O P E R A T O R S
//Some comparison operators check whether one value is greater or less than to the other value.
	let a = 50;
	let b = 65;

	console.log("Start of Relational Operators");

	//GT or Greater Than operator (>)
	 let isGreaterThan = a > b;

	//LT or Less Than operator (<)
	 let isLessThan = a < b;

	//GTE or Greater Than or Equals operator (>=)
	 let isGTorEqual = a >= b;

	//LT or Less Than or Equalsoperator (<=)
	 let isLTorEqual = a <= b;

	console.log(isGreaterThan);
	console.log(isLessThan);
	console.log(isGTorEqual);
	console.log(isLTorEqual);

	let numStr = '30';
	console.log(a > numStr); //true - converted string to a number
	console.log(b <= numStr); //false
	
	let str = "twenty";
	//console.log(b >= str); //false - str resulted to NaN string is not numeric

//SECTION: L O G I C A L  O P E R A T O R S
	let isLegalAge = true;
	let isRegistered = false;

	//AND Operator (&& - Double Ampersand)
	 // Returns true if all operands are true
	 /*
		Same concept with multiplication
			1 * 0 = 0
			1 * 1 = 1
			0 * 1 = 0
			0 * 0 = 0
	 */
	  let allRequirementsMet = isLegalAge && isRegistered;
	  console.log("Result of logical AND operator: " + allRequirementsMet);

	//OR Operator (|| - Double Pipe/Bar)
	 // Returns true if one of the operands are true
	 /*
		Same concept with addition
			1 + 0 = 1
			1 + 1 = 1
			0 + 1 = 1
			0 + 0 = 0
	 */
	  let someRequirementsMet = isLegalAge || isRegistered;
	  console.log("Result of logical AND operator: " + someRequirementsMet);

	//NOT Operator (! Exclamation Point)
	 // Returns the opposite value
	  let someRequirementsNotMet = !isRegistered;
	  console.log("Result of logical NOT operator: " + someRequirementsNotMet);



