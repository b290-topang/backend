//Structure
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

//Mongoose connecting to mongodb
mongoose.connect("mongodb+srv://admin:admin123@zuitt.grs8r9i.mongodb.net/b290-to-do?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true })


let db = mongoose.connection;
db.on("error", console.error.bind(console, "DB connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

const Task = mongoose.model("Task", taskSchema)



app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//findOne
app.post("/newTask", (req, res) => {

	Task.findOne({ name: req.body.name }).then((result, err) => {
		if (result != null && result.name == req.body.name) {
				return res.send("Duplicate task found");

		} else {
			let newTask = new Task ({
				name: req.body.name
			});

			newTask.save().then((savedTask, savedErr) => {
				if (savedErr) {
					return console.log(savedErr)

				} else {
					return res.status(201).send("New task created");

				}
			})
		}
	})
})

//find All
app.get("/tasks", (req, res) => {
	Task.find().then(tasks => {
		res.status(200).json({
			data: tasks
		});

	}).catch (error => res.send(error))
})


//Listen
if(require.main === module) {
	app.listen(port, () => console.log(`Server running at localhost:${port}.`));

}

module.exports = app;