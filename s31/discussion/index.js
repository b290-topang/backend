// Use the "require" directive to load node.js
 // https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview

// A "package" or "module" is a software component or part of a program that contains one or more routine
// The "http module" lets node.js transfer data using Hyper Text Transfer Protocol
// Clients (browser) and servers (node/express) communicate by exchanging individual messages

let http = require("http");

// Using this module "createServer()" method, we can create an HTTP SERVER that listens on a specific port.

// A port is a virtual point where network connection start and end.

// Each port is associated with specific process or serves

 // https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers

http.createServer(function (request, response) {
	// writeHead() method
	// Set the status code for the response - 200 -> OK/SUCCESS

	response.writeHead(200, {"Content-Type": "text/plain"});

	// Send the response with the text content "Hello World!"

	response.end("Hello World!");

}).listen(4000);

// Whenever the server starts, console will print the message in our "terminal"
console.log("Server running at localhost:4000")