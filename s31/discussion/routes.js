const http = require("http");

// Creates a variable "port" to store the port number
const port = 4001;

// Creates a variable "app" that stores the output of "createServe()" method.

const app = http.createServer((request, response) => {
	if(request.url == "/greeting"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Hello Again!");

	} else if (request.url == "/homepage") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("This is the homepage.");

	} else {
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("404: Page not found!");
	}
	
});

// Uses the "app" and "port" variables created above
app.listen(port);

console.log(`Server now running at localhost:${port}.`);

	// installing nodemon in gitbash
	 // npm i -g nodemon