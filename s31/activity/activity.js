// 1. What directive is used by Node.js in loading the modules it needs?
	
	// http module
	// require("http");
	 // const http = require("http");

// 2. What Node.js module contains a method for server creation?
	
	// http module
	// require("http");
	 // const http = require("http");

// 3. What is the method of the http object responsible for creating a server using Node.js?

	// http.createServer()
	 // http.createServer(function (request, response) {});

// 4. What method of the response object allows us to set status codes and content types?
	
	// response.writeHead({}), response.end();
	 /* response.writeHead(200, {"Content-Type": "text/plain"});
	    response.end("404: Page not found!"); */

	// response.status();
	 // response.status(404).send("Not Found");

// 5. Where will console.log() output its contents when run in Node.js?
	
	// GitBash
	 //console.log(`Server now running at localhost:${port}.`);

// 6. What property of the request object contains the address' endpoint?
	
	// .listen();
	 // app.listen(port);