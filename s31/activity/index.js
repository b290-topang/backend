//Add solution here
	const http = require("http");
	const port = 3000

	const app = http.createServer((request, response) => {
		if(request.url == "/login"){
			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("Welcome to the login page.");

		} else {
			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("I'm sorry for the page you are looking for cannot be found.");

		}

	});

	app.listen(port);

	// Message to check if the app is working.
	console.log(`Server now running at localhost:${port}.`);

//Do not modify
module.exports = {app}