// Setting up the Dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js");

// Setting up the Server
const app = express();
const port = 4000;

// Database Connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.grs8r9i.mongodb.net/b290-to-do?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });

let db = mongoose.connection;

db.on("error", console.error.bind(console, "DB connection error"));
db.once("open", () => console.log("MongoDB Atlas connected."));


// Setting up the Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Setting up the Routes
app.use("/tasks", taskRoute);


if(require.main === module) {
	app.listen(port, () => console.log(`Server running at localhost:${port}.`));

}
module.exports = app;