const express = require("express");
const router = express.Router();

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js");

// Route for creatinng a course

router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if (userData.isAdmin == false)
	{
		res.send("User is not an admin!")
	} else {
		courseController.addCourse(req.body).then(result => res.send(result));
	}
})

// For admin
router.get("/all", auth.verify, (req, res) => { // need for middleware
	const userData = auth.decode(req.headers.authorization)
	if (userData.isAdmin == false)
	{
		res.send("User is not an admin!")
	} else {	
		courseController.getAllCourses().then(result => res.send(result))
	}
})

// For everyone
router.get("/", (req, res) => {
		courseController.getAllActiveCourses().then(result => res.send(result))
	})

// Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url

router.get("/:courseId", (req, res) => {
	console.log(req.params);

	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
		// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
		// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route
	courseController.getCourse(req.params).then(result => res.send(result))
})

// Route for updating a course
// JWT verification is needed for this route to ensure that a user is logged in before updating a course

router.put("/:courseId", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if (isAdmin) {
		courseController.updateCourse(req.params, req.body).then(result => res.send(result))
	} else {
		res.send("User is not an admin!")
	}
})

// Activity
// PATCH

router.patch("/:courseId/archive", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if (isAdmin) {
		courseController.archiveCourse(req.params, req.body).then(result => res.send(result))
	} else {
		res.send("User is not an admin!")
	}
})
module.exports = router;

