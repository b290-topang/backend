const Course = require("../models/Course.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then(course => true).catch(err => false)
}

module.exports.getAllCourses = () => {
	return Course.find().then(result => result).catch(error => false)
}

module.exports.getAllActiveCourses = () => {
	return Course.find({ isActive: true }).then(result => result).catch(error => false)
}

module.exports.getCourse = (reqBody) => {
	return Course.findById( reqBody.courseId ).then(result => result).catch(error => false)
}

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateCourse = (reqParams, reqBody) => {
	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(course => true).catch(error => false)
}

module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then(course => true).catch(error => false)
}