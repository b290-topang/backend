//Add code here
	const http = require("http");
	const port = 4000;

	const app = http.createServer(function (request, response){
		// 4.a. => http://localhost:4000
		if(request.url === "/" && request.method === "GET"){
			response.writeHead(200, { "Content-Type": "text/plain"});
			response.end("Welcome to Booking System!");

		// 4.b. => http://localhost:4000/profile
		} else if(request.url === "/profile" && request.method === "GET"){
			response.writeHead(200, { "Content-Type": "text/plain"});
			response.end("Welcome to your profile!");

		// 4.c. => http://localhost:4000/courses
		} else if(request.url === "/courses" && request.method === "GET"){
			response.writeHead(200, { "Content-Type": "text/plain"});
			response.end("Here's our courses available.");

		// 4.d. => http://localhost:4000/addCourse
		} else if(request.url === "/addCourse" && request.method === "POST"){
			response.writeHead(200, { "Content-Type": "text/plain"});
			response.end("Add a course to our resources.");

		// 4.e.	=> http://localhost:4000/updateCourse
		}  else if(request.url === "/updateCourse" && request.method === "PUT"){
			response.writeHead(200, { "Content-Type": "text/plain"});
			response.end("Update a course to our resources.");

		// 4.f. => http://localhost:4000/archiveCourses
		} else if(request.url === "/archiveCourses" && request.method === "DELETE"){
			response.writeHead(200, { "Content-Type": "text/plain"});
			response.end("Archive courses to our resources.");

		// 404 error
		} else {
			response.writeHead(404, { "Content-Type": "text/plain"});
			response.end("404: Page not found!");

		}

	});


//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;